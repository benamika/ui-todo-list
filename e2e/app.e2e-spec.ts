import { UITodoListPage } from './app.po';

describe('ui-todo-list App', () => {
  let page: UITodoListPage;

  beforeEach(() => {
    page = new UITodoListPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
