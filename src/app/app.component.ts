import { Component } from '@angular/core';
import {Todo} from './todo';
import {TodoDataService} from './todo-data.service';

@Component({ 
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [TodoDataService]
})
export class AppComponent {
  newTodo: Todo = new Todo();
  allChecked:boolean = false;
  complited: Number = 0;
  constructor(private todoDataService: TodoDataService) {
  }
    addTodo() {
    this.todoDataService.addTodo(this.newTodo);
    this.newTodo = new Todo();
    this.countNotCompleted();
  }

  toggleTodoComplete(todo) {

    this.todoDataService.toggleTodoComplete(todo);
    this.countNotCompleted();
  }

  removeTodo(todo) {
    this.todoDataService.deleteTodoById(todo.id);
  }

  get todos() {
    return this.todoDataService.getAllTodos();
  }
  removeComplitedTodos() {
    this.todoDataService.deleteTodoByCompleted();
  }

  markAll(allChecked){
    console.log(allChecked);
    this.todoDataService.toogleAllTodosComplete(allChecked);
    this.countNotCompleted();
  }
  countNotCompleted(){
     this.complited = this.todoDataService.countNotCompleted();
  }
}
